CREATE EXTENSION IF NOT EXISTS pgcrypto;

DROP TABLE IF EXISTS objects CASCADE;
DROP TABLE IF EXISTS apartments CASCADE;
DROP TABLE IF EXISTS houses CASCADE;
DROP TABLE IF EXISTS apartments_houses_map CASCADE;
DROP TABLE IF EXISTS objects_houses_map CASCADE;
DROP TABLE IF EXISTS posts CASCADE;
DROP TABLE IF EXISTS workers CASCADE;
DROP TABLE IF EXISTS objects_workers_map CASCADE;
DROP TABLE IF EXISTS customers CASCADE;
DROP TABLE IF EXISTS apartments_customers_map CASCADE;


--creating tables
CREATE TABLE IF NOT EXISTS objects(
	id TEXT DEFAULT gen_random_uuid() PRIMARY KEY, 
	title VARCHAR(50) NOT NULL UNIQUE,
	street VARCHAR(50) NOT NULL, 
	country VARCHAR(50) NOT NULL, 
	city VARCHAR(50) NOT NULL, 
    UNIQUE(street, country, city)
);

CREATE TABLE IF NOT EXISTS apartments(
	id TEXT DEFAULT gen_random_uuid() PRIMARY KEY, 
	title VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS houses(
	id TEXT DEFAULT gen_random_uuid() PRIMARY KEY, 
	title VARCHAR(50) NOT NULL, 
	stories INT NOT NULL,
    UNIQUE(title, stories)
);

CREATE TABLE IF NOT EXISTS apartments_houses_map(
	apartment_id TEXT REFERENCES apartments(id) NOT NULL,
	house_id TEXT REFERENCES houses(id) NOT NULL
);

CREATE TABLE IF NOT EXISTS objects_houses_map(
	object_id TEXT REFERENCES objects(id) NOT NULL,
	house_id TEXT REFERENCES houses(id) NOT NULL
);

CREATE TABLE IF NOT EXISTS posts(
	id TEXT DEFAULT gen_random_uuid() PRIMARY KEY, 
	title VARCHAR(50) NOT NULL UNIQUE, 
	salary FLOAT NOT NULL
);

CREATE TABLE IF NOT EXISTS workers(
	id TEXT DEFAULT gen_random_uuid() PRIMARY KEY, 
	first_name VARCHAR(50) NOT NULL, 
	last_name VARCHAR(50) NOT NULL, 
	post_id TEXT NOT NULL REFERENCES posts(id)
);

CREATE TABLE IF NOT EXISTS house_workers_map(
	house_id TEXT REFERENCES houses(id) NOT NULL,
	worker_id TEXT REFERENCES workers(id) NOT NULL
);

CREATE TABLE IF NOT EXISTS customers(
	id TEXT DEFAULT gen_random_uuid() PRIMARY KEY, 
	first_name VARCHAR(50) NOT NULL, 
	last_name VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS apartments_customers_map(
	apartment_id TEXT REFERENCES apartments(id) NOT NULL,
	customer_id TEXT REFERENCES customers(id) NOT NULL
);


--creating functions to find record id by title
CREATE OR REPLACE FUNCTION get_apartment_id_by_title(apartment_title varchar(255)) RETURNS text
    LANGUAGE plpgsql AS
$$
BEGIN
    RETURN(SELECT id FROM apartments WHERE title = apartment_title);
END
$$;

CREATE OR REPLACE FUNCTION get_house_id_by_title(house_title varchar(255)) RETURNS text
    LANGUAGE plpgsql AS
$$
BEGIN
    RETURN(SELECT id FROM houses WHERE title = house_title);
END
$$;

CREATE OR REPLACE FUNCTION get_object_id_by_title(object_title varchar(255)) RETURNS text
    LANGUAGE plpgsql AS
$$
BEGIN
    RETURN(SELECT id FROM objects WHERE title = object_title);
END
$$;

CREATE OR REPLACE FUNCTION get_post_id_by_title(post_title varchar(255)) RETURNS text
    LANGUAGE plpgsql AS
$$
BEGIN
    RETURN(SELECT id FROM posts WHERE title = post_title);
END
$$;

CREATE OR REPLACE FUNCTION get_worker_id_by_name(worker_first_name varchar(255), worker_last_name varchar(255)) RETURNS text
    LANGUAGE plpgsql AS
$$
BEGIN
    RETURN(SELECT id FROM workers WHERE first_name = worker_first_name AND last_name = worker_last_name);
END
$$;

CREATE OR REPLACE FUNCTION get_customer_id_by_name(customer_first_name varchar(255), customer_last_name varchar(255)) RETURNS text
    LANGUAGE plpgsql AS
$$
BEGIN
    RETURN(SELECT id FROM customers WHERE first_name = customer_first_name AND last_name = customer_last_name);
END
$$;


--creating procedures to easily map things
CREATE OR REPLACE PROCEDURE assign_apartment_to_house(apartment_title varchar(255), house_title varchar(255))
    LANGUAGE SQL AS
$$
INSERT INTO apartments_houses_map(apartment_id, house_id)
VALUES(get_apartment_id_by_title(apartment_title), get_house_id_by_title(house_title));
$$;

CREATE OR REPLACE PROCEDURE assign_house_to_object(house_title varchar(255), object_title varchar(255))
    LANGUAGE SQL AS
$$
INSERT INTO objects_houses_map(object_id, house_id)
VALUES(get_object_id_by_title(object_title), get_house_id_by_title(house_title));
$$;

CREATE OR REPLACE PROCEDURE add_new_worker(first_name varchar(255), last_name varchar(255), post_title varchar(255))
    LANGUAGE SQL AS
$$
INSERT INTO workers(first_name, last_name, post_id)
VALUES(first_name, last_name, get_post_id_by_title(post_title));
$$;

CREATE OR REPLACE PROCEDURE assign_worker_to_house(first_name varchar(255), last_name varchar(255), house_title varchar(255))
    LANGUAGE SQL AS
$$
INSERT INTO house_workers_map(house_id, worker_id)
VALUES(get_house_id_by_title(house_title), get_worker_id_by_name(first_name, last_name));
$$;

CREATE OR REPLACE PROCEDURE assign_customer_to_apartment(first_name varchar(255), last_name varchar(255), apartment_title varchar(255))
    LANGUAGE SQL AS
$$
INSERT INTO apartments_customers_map(apartment_id, customer_id)
VALUES(get_apartment_id_by_title(apartment_title), get_customer_id_by_name(first_name, last_name));
$$;


--inserting some values
INSERT INTO objects (title, street, city, country) 
VALUES ('object 1', 'Soborny', 'Zaporizhzhya', 'Ukraine'), ('object 2', 'Shkilna', 'Zaporizhzhya', 'Ukraine');

INSERT INTO apartments (title) 
VALUES ('apart 1'), ('apart 2'), ('apart 3'), ('apart 4');

INSERT INTO houses (title, stories) 
VALUES ('house 1', 4), ('house 2', 7), ('house 3', 30);

INSERT INTO posts (title, salary) 
VALUES ('worker', 1000), ('head worker', 2000), ('manager', 3000);

INSERT INTO customers (first_name, last_name) 
VALUES ('Han', 'Solo'), ('Darth', 'Vader'), ('Luke', 'Skywalker');


--assigning apartments to houses
CALL assign_apartment_to_house('apart 1', 'house 1');
CALL assign_apartment_to_house('apart 2', 'house 1');
CALL assign_apartment_to_house('apart 3', 'house 2');
CALL assign_apartment_to_house('apart 4', 'house 3');

--assigning houses to objects
CALL assign_house_to_object('house 1', 'object 1');
CALL assign_house_to_object('house 2', 'object 1');
CALL assign_house_to_object('house 3', 'object 2');

--adding new workers
CALL add_new_worker('Jeremy', 'Clarkson', 'worker');
CALL add_new_worker('Richard', 'Hammond', 'head worker');
CALL add_new_worker('James', 'May', 'manager');

--assigning workers to houses
CALL assign_worker_to_house('Jeremy', 'Clarkson', 'house 1');
CALL assign_worker_to_house('Richard', 'Hammond', 'house 1');
CALL assign_worker_to_house('James', 'May', 'house 3');

--assigning customers to apartments
CALL assign_customer_to_apartment('Darth', 'Vader', 'apart 1');
CALL assign_customer_to_apartment('Luke', 'Skywalker', 'apart 2');


--selecting some values from tables
SELECT 
    apartments.title as apartment_title,
    houses.title as house_title,
    objects.title as object_title,
    objects.city,
    customers.first_name,
    customers.last_name
    FROM apartments
    LEFT JOIN apartments_houses_map ON apartments.id = apartments_houses_map.apartment_id
    LEFT JOIN houses ON houses.id = apartments_houses_map.house_id
    LEFT JOIN objects_houses_map ON houses.id = objects_houses_map.house_id
    LEFT JOIN objects ON objects.id = objects_houses_map.object_id
    LEFT JOIN apartments_customers_map ON apartments_customers_map.apartment_id = apartments.id
    LEFT JOIN customers ON customers.id = apartments_customers_map.customer_id
